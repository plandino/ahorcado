SOURCES = main.c
FLAGS = -Wall -pedantic
CC = gcc
APPNAME = ahorcado.exe

all:
	$(CC) $(FLAGS) $(SOURCES) -o $(APPNAME)	
