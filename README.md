Objetivo:

	Juego simple de ahorcado para practicar git.

Compilaci�n:
	desde git bash simplemente ejecutar mingw32-make.exe

Pasos de desarrollo posible:

	1) fork and compile
	2) mostrar palabra.
	3) tener un array de palabras hardcodeado 
	4) elegir una de esas palabras aleatoriamente (usando seed y time.h)
	5) leer numero fijo de palabras de archivo (sobreescribir las harcodeadas y no pasarse del limite del array)
	6) reemplazar el array fijo por un array dinamico (malloc y free MUCHO CUIDADO aca, habr�a que usar valgrind para fijarse que no haya perdida de memoria)
	
	7) (En realidad se puede hacer antes) crear una funcion que muestre las palabras parcialmente en base
	a alguna l�gica que parezca eficiente.
	8) preguntarle al usuario letras durante un numero especifico de turnos
	9) mostrar las letras elegidas
	10) Impedir elegir las ya elegidas
	11) fijarse si aparecen o no en la palabra.

	12) etc, etc

Idealmente, trabajando de a mas de uno, se separar�an tareas que puedan ser desarrolladas en paralelo.  
Se podr�a hardcodear funciones para ciertos comportamientos hasta unir ramas con las implementaciones definitivas.
Se puede ver de manera mucho mas profunda pero por ahora dej�moslo as�, simple, como venga y observamos los errores  y conveniencias de los diferentes planes de desarrollo.
